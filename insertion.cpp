//  algorithm study
//  insertion sort
//  input: number of keys N and keys (1 <= N <= 30,000)
//  output: keys in decreasing order

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(void) {
    int     n_keys;
    int*    keys = NULL;
    int     key;

    printf("Input the number of keys: ");
    scanf("%d", &n_keys);
    if (n_keys < 0 || n_keys > 30000) {
        printf("The range of key number must be between 1 and 30,000\n");
        return 1;
    }
    keys = (int*)malloc(n_keys * sizeof(int));

    printf("Input keys fitting the number\n");
    for (int i = 0; i < n_keys; ++i) {
        scanf("%d", &keys[i]);
    }
    printf("Keys are saved\n");

    printf("\nCalculating...\n\n");
    sleep(3);

    for (int i = 1; i < n_keys; ++i) {
        key = keys[i];

        int j = i - 1;
        while (j >= 0 && keys[j] < key) {
            keys[j+1] = keys[j];
            --j;
        }
        keys[j+1] = key;
    }

    printf("Result\n");
    for (int i = 0; i < n_keys; ++i) {
        printf("%d\n", keys[i]);
    }

    free(keys);
    keys = NULL;
    return 0;
}