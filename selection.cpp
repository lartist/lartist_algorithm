//  algorithm study
//  selection sort
//  input: number of keys M, step M and keys (1 <= N, M <= 30,000)
//  output: keys after M steps of the selection sort (increasing order)

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void swap(int*, int*);

int main(void) {
    int     n_keys;
    int     n_steps;
    int*    keys = NULL;
    int     i_min;

    printf("Input the number of keys & steps: ");
    scanf("%d %d", &n_keys, &n_steps);
    if (n_keys < 1 || n_keys > 30000 || n_steps < 1 || n_steps > 30000) {
        printf("The range of key and step number must be between 1 and 30,000\n");
        return 1;
    }
    keys = (int*)malloc(n_keys * sizeof(int));

    printf("Input keys fitting the number\n");
    for (int i = 0; i < n_keys; ++i) {
        scanf("%d", &keys[i]);
    }
    printf("Keys are saved\n");

    printf("\nCalculating...\n\n");
    sleep(3);

    for (int i = 0; i < n_steps; ++i) {
        i_min = i;
        for (int j = i; j < n_keys; ++j) {
            if (keys[j] < keys[i_min]) {
                i_min = j;
            }
        }
        swap(&keys[i], &keys[i_min]);
    }

    printf("Result\n");
    for (int i = 0; i < n_keys; ++i) {
        printf("%d\n", keys[i]);
    }

    free(keys);
    keys = NULL;
    return 0;
}

void swap(int* a, int* b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}