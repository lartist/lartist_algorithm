//  algorithm study
//  priority queue
//  input: 0 (print output, exit) // 1 a (insert a) // 2 (extract max) // 3 i b (substitute A[i] with b and restore heap)
//  output: extracted elements // current heap

#include <stdio.h>
#include <stdlib.h>

int main(void) {
    int     choice;
    int     v_new;
    int     i_target;
    int*    keys = NULL;
    int     n_keys = 0;
    int*    extracted = NULL;
    int     n_extracted = 0;

    while (1) {
        printf("\n>>> ");
        scanf("%d", &choice);

        switch(choice) {
            case 0:     // print output and exit

            for (int i = 0; i < n_extracted; ++i) {
                printf("%d ", extracted[i + 1]);
            }
            printf("\n");
            for (int i = 0; i < n_keys; ++i) {
                printf("%d ", keys[i + 1]);
            }
            printf("\n");

            if (extracted != NULL) {
                free(extracted);
                extracted = NULL;
            }
            if (keys != NULL) {
                free(keys);
                keys = NULL;
            }

            return 0;

            case 1:     // insert a
            scanf("%d", &v_new);

            break;

            case 2:     // extract max
            break;

            case 3:     // substitute keys[i] with b
            scanf("%d %d", &i_target, &v_new);
            break;

            default:
            printf("Wrong input");
            break;
        }
    }
    
}