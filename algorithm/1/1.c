#include <stdio.h>

#define K_MAX 100
#define ARR_SIZE_MAX 100
#define ELEMENT_INFINITE 1000
#define ELEMENT_LEN_MAX 3

int main(void) {
	FILE* in;
	int data[K_MAX][ARR_SIZE_MAX];
	int result[K_MAX * ARR_SIZE_MAX];
	int heap[K_MAX];
	int k;
	char buffer[(ELEMENT_LEN_MAX + 1) * K_MAX];

	in = fopen("input.txt", "r");
	fscanf(in, "%d", &k);

	for (int i = 0; i < k; ++i) {
		fscanf(in, "%[^\n]s", buffer);

		for (int j = 0; j < K_MAX; ++j) {
			fscanf(buffer, "%d", &data[i][j]);
		}
			
	}
