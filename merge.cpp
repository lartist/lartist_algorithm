//  algorithm study
//  merge sort
//  input: number of keys N and keys (1 <= N <= 100,000)
//  output: keys in decreasing order

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void mergeSort(int*, int, int);
void combine(int*, int, int, int);

int main(void) {
    int     n_keys;
    int*    keys = NULL;
    int     key;

    printf("Input the number of keys: ");
    scanf("%d", &n_keys);
    if (n_keys < 0 || n_keys > 100000) {
        printf("The range of key number must be between 1 and 100,000\n");
        return 1;
    }
    keys = (int*)malloc(n_keys * sizeof(int));

    printf("Input keys fitting the number\n");
    for (int i = 0; i < n_keys; ++i) {
        scanf("%d", &keys[i]);
    }
    printf("Keys are saved\n");

    printf("\nCalculating...\n\n");
    sleep(3);

    mergeSort(keys, 0, n_keys);

    printf("Result\n");
    for (int i = 0; i < n_keys; ++i) {
        printf("%d\n", keys[i]);
    }

    free(keys);
    keys = NULL;
    return 0;
}

void mergeSort(int* keys, int from, int to) {
    if (to > from + 1) {
        int middle = (from + to) / 2;
        mergeSort(keys, from, middle);
        mergeSort(keys, middle, to);
        combine(keys, from, middle, to);
    }
}

void combine(int* keys, int from, int middle, int to) {
    int* left = (int*)malloc((middle - from + 1) * sizeof(int));
    int* right = (int*)malloc((to - middle + 1) * sizeof(int));

    for (int i = 0; i < middle - from; ++i) {
        left[i] = keys[from + i];
    }
    left[middle - from] = -1;
    
    for (int i = 0; i < to - middle; ++i) {
        right[i] = keys[middle + i];
    }
    right[to - middle] = -1;

    int l = 0, r = 0;
    for (int i = from; i < to; ++i) {
        if (left[l] > right[r]) {
            keys[i] = left[l];
            ++l;
        } else {
            keys[i] = right[r];
            ++r;
        }
    }

    free(left);
    left = NULL;
    free(right);
    right = NULL;
}