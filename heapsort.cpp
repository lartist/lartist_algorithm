//  algorithm study
//  heap sort
//  input: input key N (1 <= N <= 100,000), number k (1 <= k <= 30) // N keys
//  output: k biggest elements in decreasing order // current heap

#include <stdio.h>
#include <stdlib.h>

void swap(int*, int*);
void maxHeapify(int*, int, int);

int main(void) {
    int     N, k;
    int*    keys;

    printf("Input N, k: ");
    scanf("%d %d", &N, &k);
    if (N < 1 || N > 100000 || k < 1 || k > 30) {
        printf("Wrong input\n");
        return 1;
    }

    keys = (int*)malloc((N + 1) * sizeof(int));
    
    printf("Input N keys\n");
    for (int i = 1; i < N + 1; ++i) {
        scanf("%d", &keys[i]);
    }

    maxHeapify(keys, N, 1);

    for (int i = 0; i < k; ++i) {
        printf("%d ", keys[1]);
        swap(&keys[1], &keys[N]);
        N -= 1;
        maxHeapify(keys, N, 1);
    }
    printf("\n");

    for (int i = 1; i < N + 1; ++i) {
        printf("%d ", keys[i]);
    }
    printf("\n");

    if (keys != NULL) {
        free(keys);
        keys = NULL;
    }

    return 0;
}

void swap(int* a, int* b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}

void maxHeapify(int* keys, int N, int parent) {
    int     left = parent * 2;
    int     right = left + 1;
    
    if (parent <= N / 2) {
        if (keys[left] > keys[parent]) {
            if (keys[right] > keys[left]) {
                swap(&keys[parent], &keys[right]);
                maxHeapify(keys, N, right);
            } else {
                swap(&keys[parent], &keys[left]);
                maxHeapify(keys, N, left);
            }
        } else if (keys[right] > keys[parent]) {
            swap(&keys[parent], &keys[right]);
            maxHeapify(keys, N, left);
        }
    }
}